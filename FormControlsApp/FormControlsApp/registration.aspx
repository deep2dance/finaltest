﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registration.aspx.cs" Inherits="FormControlsApp.registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Emoloyee new registration</title>
    <link type="text/css" href="Content/Site.css" rel="stylesheet" />
</head>
<body>
    
    <div style="margin:5px 55px;padding:0;">
        <form id="form1" runat="server">
        <asp:Table runat="server">
          <asp:TableRow>
              <asp:TableCell> <asp:Label ID="lblename" runat="server" Text="lblemployeename">Employee Name</asp:Label></asp:TableCell>
              <asp:TableCell><asp:TextBox ID="txtename" runat="server" placeholder="Name"></asp:TextBox></asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
                 <asp:TableCell><asp:Label ID="lbledesg" runat="server" Text="lbldesg">Designation</asp:Label></asp:TableCell>
                 <asp:TableCell><asp:TextBox ID="txtedesg" runat="server" placeholder="Designation"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
          <asp:TableRow>
              <asp:TableCell> <asp:Label ID="Label1" runat="server" Text="lblempname">e-Mail</asp:Label></asp:TableCell>
              <asp:TableCell><asp:TextBox ID="txtmail" runat="server" placeholder="e-Mail"></asp:TextBox></asp:TableCell>
          </asp:TableRow>
          <asp:TableRow> 
                 <asp:TableCell><asp:Label ID="Label2" runat="server" Text="lbldesg">Password</asp:Label></asp:TableCell>
                 <asp:TableCell><asp:TextBox  ID="txtpass" runat="server" placeholder="Password"></asp:TextBox></asp:TableCell>
                 
             </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2"><asp:CheckBox ID="CheckBox1" runat="server" /> I agree to the Terms &amp; Conditions</asp:TableCell>
            </asp:TableRow>
          <asp:TableRow> 
                 <asp:TableCell ColumnSpan="2"><asp:Button ID="btnsub" runat="server" Text="Submit" /> </asp:TableCell>
                 
          </asp:TableRow>
        </asp:Table>
      </form>
    </div>
        
    
</body>
</html>
